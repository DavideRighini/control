#include <systemc.h>
#include "control.hpp"

void Control::update_ports() {
	bool prima=0;
	while(true) {
	
		if (prima == 0) { //inizializzazione uscite
			muxPc->write(0); //da provare
			prima=1;
		}
		
		switch (opcode->read().to_uint()) {
			case 0 : //add
				weRf->write(1);
				funcAlu->write(5); //add
				muxAlu2->write(0);
				muxAlu1->write(0); 
			  muxTgt->write(0);
			  weDmem->write(0);
			  muxRf->write(0);
			  muxPc->write(0);
				break;
			case 1: //addi
				weRf->write(1);
				funcAlu->write(5);//add
				muxAlu2->write(1); 	
				muxAlu1->write(0);	
			  muxTgt->write(0);
			  weDmem->write(0);
			  muxRf->write(0);
			  muxPc->write(0);
				break;	
			case 2: //nand
				weRf->write(1);
				funcAlu->write(15);//nand 
				muxAlu2->write(0);
				muxAlu1->write(0);			
			  muxTgt->write(0);
			  weDmem->write(0);
			  muxRf->write(0);
			  muxPc->write(0);
				break;
			case 3: //lui
				weRf->write(1);
				funcAlu->write(20);//pass 
				muxAlu1->write(1);
			  muxTgt->write(0);
			  weDmem->write(0);
			  muxRf->write(0);
			  muxPc->write(0);
				break;
			case 4: //lw
				weRf->write(1);
				weDmem->write(0);
				funcAlu->write(5);//add 			
				muxAlu2->write(1);
				muxAlu1->write(0);
			  muxTgt->write(1);
			  muxRf->write(0);
			  muxPc->write(0);
				break;
			case 5: //sw
				weRf->write(0);
				weDmem->write(1);
				funcAlu->write(5);//add 
				muxAlu2->write(1);
				muxAlu1->write(0);
			  muxTgt->write(1);
			  muxRf->write(1);
			  muxPc->write(0);
				break;				
			case 6: //bne
				weRf->write(0);
				funcAlu->write(10);//sub 
				weDmem->write(0);
				muxAlu2->write(0);
				muxAlu1->write(0);
			  muxTgt->write(1);
			  muxRf->write(1);	
			  wait(4,SC_NS);	
				if (unEq->read() ) {
					muxPc->write(1); 
				} else {
					muxPc->write(0); 
				}	
						  		
				break;
			case 7: //jalr
				weRf->write(1);		
				muxPc->write(2);	
				funcAlu->write(20);//pass
				weDmem->write(0);
				muxAlu2->write(0);
				muxAlu1->write(0);
			  muxTgt->write(2);
			  muxRf->write(0);				
				break;
			default :
				cout << "error unknown opcode function\n";
		}
		wait();
	}
}
