#ifndef CONTROL_HPP
#define CONTROL_HPP

SC_MODULE(Control) {
	
	static const unsigned DATA_OPCODE = 3;
	static const unsigned DATA_ALU = 6;
	static const unsigned DATA_MUXPC = 2;
	static const unsigned DATA_MUXTGT = 2;
	
	sc_in<bool> unEq;	
	sc_in<sc_lv<DATA_OPCODE> > opcode;
	sc_out<sc_lv<DATA_ALU> > funcAlu;
	sc_out<sc_lv<DATA_MUXPC> > muxPc;
	sc_out<sc_lv<DATA_MUXTGT> > muxTgt;
	sc_out<bool> muxAlu1;
	sc_out<bool> muxAlu2;
	sc_out<bool> weDmem;
	sc_out<bool> weRf;
	sc_out<bool> muxRf;
	
	
	SC_CTOR(Control) {
		SC_THREAD(update_ports);
			sensitive << opcode << unEq;
			dont_initialize();
	}
	
	private:
	void update_ports();
	
};

#endif
