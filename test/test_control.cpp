#include <systemc.h>
#include "control.hpp"

#include <iostream>
#include <fstream>
#include <string>

using namespace std;

static const unsigned TEST_SIZE = 6;
static const unsigned DATA_WIDTH = 16;
static const unsigned DATA_OPCODE = 3;
static const unsigned DATA_ALU = 6;
static const unsigned DATA_MUXPC = 2;
static const unsigned DATA_MUXTGT = 2;
sc_fifo<bool> observer_control_weRf;
sc_fifo<unsigned> observer_control_funcAlu;
sc_fifo<bool> observer_control_unEq;
sc_fifo<unsigned> observer_control_muxPc;
sc_fifo<unsigned> observer_control_muxTgt;
sc_fifo<bool> observer_control_muxAlu1;
sc_fifo<bool> observer_control_muxAlu2;
sc_fifo<bool> observer_control_weDmem;
sc_fifo<bool> observer_control_muxRf;
int values1[TEST_SIZE]; //vettore usato per tutti i test
int values2[TEST_SIZE]; //vettore usato per tutti i test
sc_lv<DATA_OPCODE> values3[TEST_SIZE]; //vettore usato per i test del blocco control

SC_MODULE(TestBench) 
{
	public:
	//canali per Control
	sc_signal<bool> control_unEq;
	sc_signal<sc_lv<DATA_OPCODE> > control_opcode;
	sc_signal<sc_lv<DATA_ALU> > control_funcAlu;	
	sc_signal<sc_lv<DATA_MUXPC> > control_muxPc;
	sc_signal<sc_lv<DATA_MUXTGT> > control_muxTgt;
	sc_signal<bool> control_muxAlu1;
	sc_signal<bool> control_muxAlu2;	
	sc_signal<bool> control_weDmem;
	sc_signal<bool> control_weRf;
	sc_signal<bool> control_muxRf; 
	
	Control control1; //CONTROL UNIT
	
  SC_CTOR(TestBench) : control1("control1")
  {
 		init_values(); 
 		
		//Control:
		SC_THREAD(init_values_control_test);
		control1.unEq(this->control_unEq);
		control1.opcode(this->control_opcode);
		control1.funcAlu(this->control_funcAlu);
		control1.muxPc(this->control_muxPc);
		control1.muxTgt(this->control_muxTgt);
		control1.muxAlu1(this->control_muxAlu1);
		control1.muxAlu2(this->control_muxAlu2);
		control1.weDmem(this->control_weDmem);
		control1.weRf(this->control_weRf);
		control1.muxRf(this->control_muxRf);		
		SC_THREAD(observer_thread_control);	
			sensitive << control1.unEq << control1.funcAlu << control1.muxPc << control1.muxTgt << control1.muxAlu1 << control1.muxAlu2 << control1.weDmem << control1.weRf << control1.muxRf; 
	}
	
	void init_values() {
				values1[0] = 5;
				values1[1] = 4;
				values1[2] = 3;
				values1[3] = 2;
				values1[4] = 1;
				values1[5] = 0;
				
				values2[0] = 1;
				values2[1] = 3;
				values2[2] = 5;
				values2[3] = 7;
				values2[4] = 9;
				values2[5] = 0;
				
				values3[0] = "000"; //add
				values3[1] = "001"; //addi
				values3[2] = "010"; //nand
				values3[3] = "011"; //lui
				values3[4] = "100"; //sw
				values3[5] = "101"; //lw
				values3[6] = "110"; //bne
				values3[7] = "111"; //jalr		  		  
	}				

	void init_values_control_test() {
		for (unsigned i=0;i<TEST_SIZE;i++) {
		  control_opcode.write(values3[i]);    
		  wait(SC_ZERO_TIME);
		  wait(10,SC_NS);
		}
		wait(50,SC_NS);
		sc_stop();
	}
	
	void observer_thread_control() {
		while(true) {
				wait();
				unsigned value = control1.opcode->read().to_uint();
				cout << "\n\nobserver_thread: at " << sc_time_stamp() << " control opcode: " << value << endl;
				cout << "observer_thread: at " << sc_time_stamp() << " control alu_func: " << control1.funcAlu->read().to_uint() << "\n";
				cout << "observer_thread: at " << sc_time_stamp() << " control weRf: " << control1.weRf->read() << "\n";
				cout << "observer_thread: at " << sc_time_stamp() << " control muxAlu1: " << control1.muxAlu1->read() << "\n";
				cout << "observer_thread: at " << sc_time_stamp() << " control muxAlu2: " << control1.muxAlu2->read() << "\n";
				cout << "observer_thread: at " << sc_time_stamp() << " control muxTgt: " << control1.muxTgt->read().to_uint() << "\n";
				cout << "observer_thread: at " << sc_time_stamp() << " control muxRf: " << control1.muxRf->read() << "\n";
				cout << "observer_thread: at " << sc_time_stamp() << " control muxPc: " << control1.muxPc->read() << "\n";
				cout << "observer_thread: at " << sc_time_stamp() << " control weDmem: " << control1.weDmem->read() << "\n\n";
				
				if (observer_control_weRf.num_free()>0 )  observer_control_weRf.write(control1.weRf->read());
				if (observer_control_funcAlu.num_free()>0 )  observer_control_funcAlu.write(control1.funcAlu->read().to_uint());
				if (observer_control_unEq.num_free()>0 )  observer_control_unEq.write(control1.unEq->read());
				if (observer_control_muxPc.num_free()>0 )  observer_control_muxPc.write(control1.muxPc->read().to_uint());
				if (observer_control_muxTgt.num_free()>0 )  observer_control_muxTgt.write(control1.muxTgt->read().to_uint());
				if (observer_control_muxAlu1.num_free()>0 )  observer_control_muxAlu1.write(control1.muxAlu1->read());
				if (observer_control_muxAlu2.num_free()>0 )  observer_control_muxAlu2.write(control1.muxAlu2->read());
				if (observer_control_weDmem.num_free()>0 )  observer_control_weDmem.write(control1.weDmem->read());
				if (observer_control_muxRf.num_free()>0 )  observer_control_muxRf.write(control1.muxRf->read());
		} 
	}


	int check_control() {
		int weRf,funcAlu,unEq,muxPc,muxTgt,muxAlu1,muxAlu2,weDmem,muxRf;
	
		for (unsigned i=0;i<TEST_SIZE;i++) {
			//cout << "test: " << i << "\n";
			weRf = observer_control_weRf.read();
			funcAlu = observer_control_funcAlu.read();
			unEq = observer_control_unEq.read();
			muxPc = observer_control_muxPc.read();
			muxTgt = observer_control_muxTgt.read();
			muxAlu1 = observer_control_muxAlu1.read();
			muxAlu2 = observer_control_muxAlu2.read();
			weDmem = observer_control_weDmem.read();
			muxRf = observer_control_muxRf.read();

			switch (values3[i].to_uint()) {
				case 0 : //add
					if (weRf != 1 & funcAlu!=5) return 1;
					break;
				case 1: //addi
					if (weRf != 1 & funcAlu!=5) return 1;			
					break;	
				case 2: //nand
					if (weRf != 1 & funcAlu!=15) return 1;			
					break;
				case 3: //lui
					if (weRf != 1 & funcAlu!=20) return 1; 
					break;
				case 4: //lw
					if (weRf != 1 & funcAlu!=5 & weDmem !=1) return 1;					
					break;
				case 5: //sw
					if (weRf != 0 & funcAlu!=5 & weDmem !=1) return 1;
					break;
				case 6: //bne
					if (unEq) {
						if (weRf != 0 & funcAlu!=10 & muxPc !=1) return 1;
					} else {
						if (weRf != 0 & funcAlu!=10 & muxPc !=0) return 1;
					}
					break;
				case 7: //jalr
					if (weRf != 1 & funcAlu!=20 & muxPc !=2) return 1;
					break;
				default :
					return 1;
			}	
		}
		return 0;
	}
};

// ---------------------------------------------------------------------------------------
		
int sc_main(int argc, char* argv[]) {

	
	TestBench testbench("testbench");
	sc_start();

  return testbench.check_control();
}
